package t4_201710;

import java.util.ArrayList;

import model.VO.VOPelicula;
import model.data_structures.DoubleLinkedList;
import model.data_structures.ILista;
import model.logic.Ordenador;
import model.logic.Pelicula;
import junit.framework.TestCase;

public class OrdenadorTest extends TestCase{
	private Ordenador virtual;
	private ArrayList<Pelicula> ej;
	private Pelicula[] ej2;
	private ILista<Pelicula> ej3;

	public void SetupEscenario1(){
		virtual = new Ordenador();
		ej = new ArrayList<>(7);

		Pelicula fst = new Pelicula();
		VOPelicula pr = new VOPelicula();
		pr.setTitulo("a");
		fst.setMovie(pr);
		ej.add(fst);

		Pelicula sec = new Pelicula();
		VOPelicula sg = new VOPelicula();
		sg.setTitulo("b");
		sec.setMovie(sg);
		ej.add(sec);

		Pelicula thr = new Pelicula();
		VOPelicula tr = new VOPelicula();
		tr.setTitulo("c");
		thr.setMovie(tr);
		ej.add(thr);

		Pelicula fr = new Pelicula();
		VOPelicula ctr = new VOPelicula();
		ctr.setTitulo("d");
		fr.setMovie(ctr);
		ej.add(fr);

		Pelicula fv = new Pelicula();
		VOPelicula cnc = new VOPelicula();
		cnc.setTitulo("e");
		fv.setMovie(cnc);
		ej.add(fv);

		Pelicula sx = new Pelicula();
		VOPelicula ss = new VOPelicula();
		ss.setTitulo("f");
		sx.setMovie(ss);
		ej.add(sx);

		Pelicula svn = new Pelicula();
		VOPelicula st = new VOPelicula();
		st.setTitulo("g");
		svn.setMovie(st);
		ej.add(svn);
	}

	public void SetupEscenario2(){
		virtual = new Ordenador();
		ej = new ArrayList<>(7);

		Pelicula fst = new Pelicula();
		VOPelicula pr = new VOPelicula();
		pr.setTitulo("g");
		fst.setMovie(pr);
		ej.add(fst);

		Pelicula sec = new Pelicula();
		VOPelicula sg = new VOPelicula();
		sg.setTitulo("f");
		sec.setMovie(sg);
		ej.add(sec);

		Pelicula thr = new Pelicula();
		VOPelicula tr = new VOPelicula();
		tr.setTitulo("e");
		thr.setMovie(tr);
		ej.add(thr);

		Pelicula fr = new Pelicula();
		VOPelicula ctr = new VOPelicula();
		ctr.setTitulo("d");
		fr.setMovie(ctr);
		ej.add(fr);

		Pelicula fv = new Pelicula();
		VOPelicula cnc = new VOPelicula();
		cnc.setTitulo("c");
		fv.setMovie(cnc);
		ej.add(fv);

		Pelicula sx = new Pelicula();
		VOPelicula ss = new VOPelicula();
		ss.setTitulo("b");
		sx.setMovie(ss);
		ej.add(sx);

		Pelicula svn = new Pelicula();
		VOPelicula st = new VOPelicula();
		st.setTitulo("a");
		svn.setMovie(st);
		ej.add(svn);
	}

	public void SetupEscenario3(){
		virtual = new Ordenador();
		ej = new ArrayList<>(7);

		Pelicula fst = new Pelicula();
		VOPelicula pr = new VOPelicula();
		pr.setTitulo("f");
		fst.setMovie(pr);
		ej.add(fst);

		Pelicula sec = new Pelicula();
		VOPelicula sg = new VOPelicula();
		sg.setTitulo("a");
		sec.setMovie(sg);
		ej.add(sec);

		Pelicula thr = new Pelicula();
		VOPelicula tr = new VOPelicula();
		tr.setTitulo("c");
		thr.setMovie(tr);
		ej.add(thr);

		Pelicula fr = new Pelicula();
		VOPelicula ctr = new VOPelicula();
		ctr.setTitulo("b");
		fr.setMovie(ctr);
		ej.add(fr);

		Pelicula fv = new Pelicula();
		VOPelicula cnc = new VOPelicula();
		cnc.setTitulo("g");
		fv.setMovie(cnc);
		ej.add(fv);

		Pelicula sx = new Pelicula();
		VOPelicula ss = new VOPelicula();
		ss.setTitulo("e");
		sx.setMovie(ss);
		ej.add(sx);

		Pelicula svn = new Pelicula();
		VOPelicula st = new VOPelicula();
		st.setTitulo("d");
		svn.setMovie(st);
		ej.add(svn);
	}
	
	public void SetupEscenario4(){
		virtual = new Ordenador();
		ej2 = new Pelicula[7];
		
		Pelicula fst = new Pelicula();
		VOPelicula pr = new VOPelicula();
		pr.setTitulo("a");
		fst.setMovie(pr);
		ej2[0]=fst;
		
		Pelicula sec = new Pelicula();
		VOPelicula sg = new VOPelicula();
		sg.setTitulo("b");
		sec.setMovie(sg);
		ej2[1]=sec;

		Pelicula thr = new Pelicula();
		VOPelicula tr = new VOPelicula();
		tr.setTitulo("c");
		thr.setMovie(tr);
		ej2[2]=thr;

		Pelicula fr = new Pelicula();
		VOPelicula ctr = new VOPelicula();
		ctr.setTitulo("d");
		fr.setMovie(ctr);
		ej2[3]=fr;

		Pelicula fv = new Pelicula();
		VOPelicula cnc = new VOPelicula();
		cnc.setTitulo("e");
		fv.setMovie(cnc);
		ej2[4]=fv;

		Pelicula sx = new Pelicula();
		VOPelicula ss = new VOPelicula();
		ss.setTitulo("f");
		sx.setMovie(ss);
		ej2[5]=sx;
		
		Pelicula svn = new Pelicula();
		VOPelicula st = new VOPelicula();
		st.setTitulo("g");
		svn.setMovie(st);
		ej2[6]=svn;		
	}
	
	public void SetupEscenario5(){
		virtual = new Ordenador();
		ej2 = new Pelicula[7];

		Pelicula fst = new Pelicula();
		VOPelicula pr = new VOPelicula();
		pr.setTitulo("g");
		fst.setMovie(pr);
		ej2[0]=fst;

		Pelicula sec = new Pelicula();
		VOPelicula sg = new VOPelicula();
		sg.setTitulo("f");
		sec.setMovie(sg);
		ej2[1]=sec;

		Pelicula thr = new Pelicula();
		VOPelicula tr = new VOPelicula();
		tr.setTitulo("e");
		thr.setMovie(tr);
		ej2[2]=thr;

		Pelicula fr = new Pelicula();
		VOPelicula ctr = new VOPelicula();
		ctr.setTitulo("d");
		fr.setMovie(ctr);
		ej2[3]=sec;

		Pelicula fv = new Pelicula();
		VOPelicula cnc = new VOPelicula();
		cnc.setTitulo("c");
		fv.setMovie(cnc);
		ej2[4]=fv;

		Pelicula sx = new Pelicula();
		VOPelicula ss = new VOPelicula();
		ss.setTitulo("b");
		sx.setMovie(ss);
		ej2[5]=sx;

		Pelicula svn = new Pelicula();
		VOPelicula st = new VOPelicula();
		st.setTitulo("a");
		svn.setMovie(st);
		ej2[6]=svn;
	}
	
	public void SetupEscenario6(){
		virtual = new Ordenador();
		ej2 = new Pelicula[7];
		
		Pelicula fst = new Pelicula();
		VOPelicula pr = new VOPelicula();
		pr.setTitulo("b");
		fst.setMovie(pr);
		ej2[0]=fst;
		
		Pelicula sec = new Pelicula();
		VOPelicula sg = new VOPelicula();
		sg.setTitulo("a");
		sec.setMovie(sg);
		ej2[1]=sec;

		Pelicula thr = new Pelicula();
		VOPelicula tr = new VOPelicula();
		tr.setTitulo("d");
		thr.setMovie(tr);
		ej2[2]=thr;

		Pelicula fr = new Pelicula();
		VOPelicula ctr = new VOPelicula();
		ctr.setTitulo("c");
		fr.setMovie(ctr);
		ej2[3]=fr;

		Pelicula fv = new Pelicula();
		VOPelicula cnc = new VOPelicula();
		cnc.setTitulo("e");
		fv.setMovie(cnc);
		ej2[4]=fv;

		Pelicula sx = new Pelicula();
		VOPelicula ss = new VOPelicula();
		ss.setTitulo("g");
		sx.setMovie(ss);
		ej2[5]=sx;
		
		Pelicula svn = new Pelicula();
		VOPelicula st = new VOPelicula();
		st.setTitulo("f");
		svn.setMovie(st);
		ej2[6]=svn;
	}
	
	public void SetupEscenario7(){
		virtual = new Ordenador();
		ej3 = new DoubleLinkedList<Pelicula>(null);
		
		Pelicula fst = new Pelicula();
		VOPelicula pr = new VOPelicula();
		pr.setTitulo("a");
		fst.setMovie(pr);
		ej3.agregarElementoFinal(fst);
		
		Pelicula sec = new Pelicula();
		VOPelicula sg = new VOPelicula();
		sg.setTitulo("b");
		sec.setMovie(sg);
		ej3.agregarElementoFinal(sec);

		Pelicula thr = new Pelicula();
		VOPelicula tr = new VOPelicula();
		tr.setTitulo("c");
		thr.setMovie(tr);
		ej3.agregarElementoFinal(thr);

		Pelicula fr = new Pelicula();
		VOPelicula ctr = new VOPelicula();
		ctr.setTitulo("d");
		fr.setMovie(ctr);
		ej3.agregarElementoFinal(fr);

		Pelicula fv = new Pelicula();
		VOPelicula cnc = new VOPelicula();
		cnc.setTitulo("e");
		fv.setMovie(cnc);
		ej3.agregarElementoFinal(fv);

		Pelicula sx = new Pelicula();
		VOPelicula ss = new VOPelicula();
		ss.setTitulo("f");
		sx.setMovie(ss);
		ej3.agregarElementoFinal(sx);
		
		Pelicula svn = new Pelicula();
		VOPelicula st = new VOPelicula();
		st.setTitulo("g");
		svn.setMovie(st);
		ej3.agregarElementoFinal(svn);
	}
	
	public void SetupEscenario8(){
		virtual = new Ordenador();
		ej3 = new DoubleLinkedList<Pelicula>(null);
		
		Pelicula fst = new Pelicula();
		VOPelicula pr = new VOPelicula();
		pr.setTitulo("g");
		fst.setMovie(pr);
		ej3.agregarElementoFinal(fst);
		
		Pelicula sec = new Pelicula();
		VOPelicula sg = new VOPelicula();
		sg.setTitulo("f");
		sec.setMovie(sg);
		ej3.agregarElementoFinal(sec);

		Pelicula thr = new Pelicula();
		VOPelicula tr = new VOPelicula();
		tr.setTitulo("e");
		thr.setMovie(tr);
		ej3.agregarElementoFinal(thr);

		Pelicula fr = new Pelicula();
		VOPelicula ctr = new VOPelicula();
		ctr.setTitulo("d");
		fr.setMovie(ctr);
		ej3.agregarElementoFinal(fr);

		Pelicula fv = new Pelicula();
		VOPelicula cnc = new VOPelicula();
		cnc.setTitulo("c");
		fv.setMovie(cnc);
		ej3.agregarElementoFinal(fv);

		Pelicula sx = new Pelicula();
		VOPelicula ss = new VOPelicula();
		ss.setTitulo("b");
		sx.setMovie(ss);
		ej3.agregarElementoFinal(sx);
		
		Pelicula svn = new Pelicula();
		VOPelicula st = new VOPelicula();
		st.setTitulo("a");
		svn.setMovie(st);
		ej3.agregarElementoFinal(svn);
	}
	
	public void SetupEscenario9(){
		virtual = new Ordenador();
		ej3 = new DoubleLinkedList<Pelicula>(null);
		
		Pelicula fst = new Pelicula();
		VOPelicula pr = new VOPelicula();
		pr.setTitulo("g");
		fst.setMovie(pr);
		ej3.agregarElementoFinal(fst);
		
		Pelicula sec = new Pelicula();
		VOPelicula sg = new VOPelicula();
		sg.setTitulo("a");
		sec.setMovie(sg);
		ej3.agregarElementoFinal(sec);

		Pelicula thr = new Pelicula();
		VOPelicula tr = new VOPelicula();
		tr.setTitulo("b");
		thr.setMovie(tr);
		ej3.agregarElementoFinal(thr);

		Pelicula fr = new Pelicula();
		VOPelicula ctr = new VOPelicula();
		ctr.setTitulo("c");
		fr.setMovie(ctr);
		ej3.agregarElementoFinal(fr);

		Pelicula fv = new Pelicula();
		VOPelicula cnc = new VOPelicula();
		cnc.setTitulo("e");
		fv.setMovie(cnc);
		ej3.agregarElementoFinal(fv);

		Pelicula sx = new Pelicula();
		VOPelicula ss = new VOPelicula();
		ss.setTitulo("d");
		sx.setMovie(ss);
		ej3.agregarElementoFinal(sx);
		
		Pelicula svn = new Pelicula();
		VOPelicula st = new VOPelicula();
		st.setTitulo("f");
		svn.setMovie(st);
		ej3.agregarElementoFinal(svn);
	}

	public void testOrdenar(){
		//Con un arreglo ya ordenado ascendentemente
		SetupEscenario1();
		ArrayList<Pelicula> actual = virtual.ordenarPeliculas(ej);
		for(int i = 0; i < ej.size(); i++){
			assertEquals(ej.get(i),actual.get(i));
		}
		
		SetupEscenario4();
		Pelicula[] actual2 = virtual.ordenarPeliculas(ej2);
		for(int i = 0; i <ej2.length; i++){
			assertEquals(ej2[i], actual2[i]);
		}
		
		SetupEscenario7();
		ILista<Pelicula> actual3 = virtual.ordenarPeliculas(ej3);
		for(int i = 0; i<ej3.darNumeroElementos(); i++){
			assertEquals(ej3.darElemento(i), actual3.darElemento(i));
		}

		//Con un arreglo en orden descendente
		SetupEscenario2();
		actual = virtual.ordenarPeliculas(ej);
		for(int i = 0; i<ej.size(); i++){
			assertEquals(ej.get(i), actual.get(i));
		}
		
		SetupEscenario5();
		actual2 = virtual.ordenarPeliculas(ej2);
		for(int i = 0; i <ej2.length; i++){
			assertEquals(ej2[i], actual2[i]);
		}
		
		SetupEscenario8();
		actual3 = virtual.ordenarPeliculas(ej3);
		for(int i = 0; i<ej3.darNumeroElementos(); i++){
			assertEquals(ej3.darElemento(i), actual3.darElemento(i));
		}

		//Con un arreglo aleatorio
		SetupEscenario3();
		actual = virtual.ordenarPeliculas(ej);
		for(int i = 0; i<ej.size(); i++){
			assertEquals(ej.get(i), actual.get(i));
		}
		
		SetupEscenario6();
		actual2 = virtual.ordenarPeliculas(ej2);
		for(int i = 0; i <ej2.length; i++){
			assertEquals(ej2[i], actual2[i]);
		}
		
		SetupEscenario9();
		actual3 = virtual.ordenarPeliculas(ej3);
		for(int i = 0; i<ej3.darNumeroElementos(); i++){
			assertEquals(ej3.darElemento(i), actual3.darElemento(i));
		}
	}
}