package controller;

import model.data_structures.ILista;
import model.logic.*;
public class Controller {
	private static GeneradorPeliculas generator = new GeneradorPeliculas();
	private static Ordenador srt = new Ordenador();
	
	public static void cargarPeliculas(){
		generator.cargarArchivoPeliculas("data/movies.csv");
	}
	public static Pelicula[] darMuestraAleatoria(int n){
		return generator.darMuestra(n);
	}
	public static Pelicula[] ordenarMuestra(Pelicula[] sample){
		long init =  System.currentTimeMillis();
		Pelicula[] resp = srt.ordenarPeliculas(sample);
        long fin = System.currentTimeMillis();
		System.out.println("START_TIME:" + init);
		System.out.println("END_TIME:" + fin);
		return resp;
	}
	public static ILista<Pelicula> darMuestraAleatoriaEnLista(int n){
		return generator.darMuestraEnLista(n);
	}
	public static ILista<Pelicula> ordenarMuestra(ILista<Pelicula> sample){
		long init =  System.currentTimeMillis();
		ILista<Pelicula> resp = srt.ordenarPeliculas(sample);
        long fin = System.currentTimeMillis();
		System.out.println("START_TIME:" + init);
		System.out.println("END_TIME:" + fin);
		return resp;
	}
}
