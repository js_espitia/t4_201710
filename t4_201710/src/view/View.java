package view;

import java.util.Scanner;

import model.data_structures.ILista;
import model.logic.Pelicula;
import controller.Controller;


public class View {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		Controller.cargarPeliculas();
		boolean fin = false;
		while(!fin){
			printMenu();
			int option = sc.nextInt();
			switch (option) {
			case 1:
				System.out.println("---------------------------------------------");
				System.out.println("�De qu� tama�o quiere que sea la muestra?");
				System.out.println("---------------------------------------------");
				String entrada = sc.next();
				try {
					int tama�o = Integer.parseInt(entrada);
					Pelicula[] muestra = Controller.darMuestraAleatoria(tama�o);
					System.out.println("---------------------------------------------");
					System.out.println("La muestra generada contiene las siguientes pel�culas:");
					for (Pelicula movie:muestra) {
						System.out.println(movie.getMovie().getTitulo() + " (" + movie.getMovie().getAgnoPublicacion() + ")");
					}
					System.out.println("---------------------------------------------");
					System.out.println("La muestra ordenada se ve as�");
					Pelicula[] ordenada = Controller.ordenarMuestra(muestra);
					/*for(Pelicula peli:ordenada){
						System.out.println(peli.getMovie().getTitulo() + " (" + peli.getMovie().getAgnoPublicacion() + ")");
					}*/
					System.out.println("---------------------------------------------");

				} catch (NumberFormatException e) {
					System.out.println("Entrada inv�lida. Debe ser un n�mero natural");
				}
				break;
			case 2:
				System.out.println("---------------------------------------------");
				System.out.println("�De qu� tama�o quiere que sea la muestra?");
				System.out.println("---------------------------------------------");
				String entrada1 = sc.next();
				try {
					int tama�o = Integer.parseInt(entrada1);
					ILista<Pelicula> muestra = Controller.darMuestraAleatoriaEnLista(tama�o);
					System.out.println("---------------------------------------------");
					System.out.println("La muestra generada contiene las siguientes pel�culas:");
					for (Pelicula movie:muestra) {
						System.out.println(movie.getMovie().getTitulo() + " (" + movie.getMovie().getAgnoPublicacion() + ")");
					}
					System.out.println("---------------------------------------------");
					System.out.println("La muestra ordenada se ve as�");
					ILista<Pelicula> ordenada = Controller.ordenarMuestra(muestra);
					/**for(Pelicula peli:ordenada){
						System.out.println(peli.getMovie().getTitulo() + " (" + peli.getMovie().getAgnoPublicacion() + ")");
					}*/
					System.out.println("---------------------------------------------");

				} catch (NumberFormatException e) {
					System.out.println("Entrada inv�lida. Debe ser un n�mero natural");
				}
				break;
			case 3:
				fin = true;
				break;
			default:
				System.out.println("-------------Entrada inv�lida---------------");
				break;
			}
		}
		sc.close();
	}

	public static void printMenu(){
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 4----------------------");
		System.out.println("1. Generar una muestra de peliculas del archivo movies.csv (Usar arreglo)");
		System.out.println("2. Generar una muestra de peliculas del archivo movies.csv (Usar lista)");
		System.out.println("3. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");
	}
}
