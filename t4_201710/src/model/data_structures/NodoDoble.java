package model.data_structures;

public class NodoDoble<T> {
	
	private T elemento;
	private NodoDoble<T> siguiente;
	private NodoDoble<T> anterior;
	
	public NodoDoble (T pElem){
		elemento = pElem;
		siguiente = null;
		anterior = null;
	}
	
	public T darElemento(){
		return elemento;
	}
	
	public NodoDoble<T> darSiguiente(){
		return siguiente;
	}
	
	public NodoDoble<T> darAnterior()
	{
		return anterior;
	}
	
	public void cambiarSiguiente(NodoDoble<T> pSiguiente){
		siguiente = pSiguiente;
	}
	
	public void cambiarAnterior(NodoDoble<T> pAnterior){
		anterior = pAnterior;
	}
	
	public void cambiarElemento(T pElem){
		elemento = pElem;
	}

}
