package model.data_structures;

public interface ILista<T> extends Iterable<T>{
	
	/**
	 * A�ade el elemento elem al final de la lista
	 * @param elem
	 */
	public void agregarElementoFinal(T elem);
	
	/**
	 * Retorna el elemento en la posici�n pos
	 * @param pos posici�n a buscar en la lista
	 * @return elemento en la posici�n pos
	 */
	public T darElemento(int pos);
	
	/**
	 * Retorna y elimina el elemento en la posici�n pos
	 * @param pos posici�n a buscar en la lista
	 * @return elemento en la posici�n pos antes de borrarlo
	 */
	public T eliminarElemento(int pos);
	
	/**
	 * Devuelve el tama�o de la lista
	 * @return n�mero de elementos en la lista
	 */
	public int darNumeroElementos();
	public void set(int pos, T elem);
	

}
