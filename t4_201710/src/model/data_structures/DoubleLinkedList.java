package model.data_structures;

import java.util.Iterator;

public class DoubleLinkedList<T> implements ILista<T> {

	private NodoDoble<T> actual;
	private NodoDoble<T> primero;
	private NodoDoble<T> ultimo;
	private int size;

	public DoubleLinkedList (NodoDoble<T> pPrimero){
		size = 0;
		primero = pPrimero;
		actual = primero;
		ultimo = primero;
		if(ultimo!=null){
			size = 1;
			while(ultimo.darSiguiente()!=null){
				ultimo = ultimo.darSiguiente();
				size++;
			}
		}
	}

	protected class MiIterador implements Iterator<T>
	{

		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			actual = (actual!=null) ? actual.darSiguiente() : primero;
			return actual!=null;
		}

		@Override
		public T next() {
			// TODO Auto-generated method stub
			//avanzarSiguientePosicion();
			return (actual == null)? null:actual.darElemento();	
		}
		@Override
		public void remove() {}
	}
	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		Iterator<T> iter = new MiIterador();
		return iter;
	}

	public void addFirstElement(T elem)
	{
		NodoDoble<T> n = new NodoDoble<T>(elem);
		if(primero ==null)
			primero = n;
		else
		{
			n.cambiarSiguiente(primero);
		}
		primero = n;
		size++;
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		int i =0;
		T resp = null;
		if (primero!= null){
			resp = primero.darElemento();
			actual = primero;
			while(i<pos){
				actual = actual.darSiguiente();
				i++;
			}
			resp = actual.darElemento();
		}
		return resp;
	}

	@Override
	public T eliminarElemento(int pos) {
		// TODO Auto-generated method stub
		int i=0;
		T resp = null;
		NodoDoble<T> anterior = null;
		NodoDoble<T> siguiente = null;
		if (primero!= null){
			resp = primero.darElemento();
			actual = primero;
			while(i<pos){
				anterior = actual;
				actual = actual.darSiguiente();
				if(actual!=null&&actual.darSiguiente()!=null)
					siguiente = actual.darSiguiente();
				i++;
			}
			resp = actual.darElemento();
			if(anterior!=null)
				anterior.cambiarSiguiente(siguiente);
			if(siguiente!=null)
				siguiente.cambiarAnterior(anterior);
		}
		return resp;
	}

	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return size;
	}

	public T removeFromTop()
	{
		T aDevolver = primero.darElemento();
		NodoDoble<T> newFirst = primero.darSiguiente();
		primero = newFirst;
		primero.cambiarAnterior(null);
		size--;
		return aDevolver;
	}

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		NodoDoble<T> nuevo = new NodoDoble<>(elem);
		if(primero == null){
			primero = nuevo;
		}
		else{
			ultimo.cambiarSiguiente(nuevo);
			nuevo.cambiarAnterior(ultimo);
		}
		ultimo = nuevo;
		size++;
	}
	
	public void set(int pos, T elem){
		int i = 0;
		if (primero!= null){
			actual = primero;
			while(i < pos){
				actual = actual.darSiguiente();
				i++;
			}
			actual.cambiarElemento(elem);
		}
	}

}
