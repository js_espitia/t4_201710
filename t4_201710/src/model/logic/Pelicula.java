package model.logic;

import model.VO.VOPelicula;

public class Pelicula implements Comparable<Pelicula> {

	private VOPelicula movie;

	public VOPelicula getMovie(){
		return movie;
	}
	public void setMovie(VOPelicula mv){
		movie = mv;
	}

	@Override
	public int compareTo(Pelicula arg0) {
		return movie.getTitulo().compareTo(arg0.getMovie().getTitulo());
	}


}
