package model.logic;


import java.io.*;

import model.VO.VOPelicula;
import model.data_structures.*;

public class GeneradorPeliculas {	

	private ILista<VOPelicula> misPeliculas;

	public GeneradorPeliculas(){
		cargarArchivoPeliculas("data/movies.csv");
	}
	public void cargarArchivoPeliculas(String archivoPeliculas) {

		misPeliculas = null;
		FileReader arg0;
		try {
			arg0 = new FileReader(new File(archivoPeliculas));
			BufferedReader in = new BufferedReader(arg0);

			misPeliculas = new DoubleLinkedList<VOPelicula>(null);

			String line = in.readLine();
			line = in.readLine();
			while(line != null){
				String[] info = new String[4];
				String  name, genres;
				if(line.indexOf('"') != -1){				
					name = line.substring(line.indexOf('"')+1,line.lastIndexOf('"')).trim();
					line = line.substring(0, line.indexOf('"'))+line.substring(line.lastIndexOf('"')+1, line.length());
					info = line.split(",");
					genres = info[1];
				}else{
					info = line.split(",");
					name = info[1].trim();
					genres = info[2];
				}
				String[] generosArr = genres.split("|");
				String titulo = name.substring(0, name.length()-7);
				String anio = "0";
				if(name.charAt(name.length()-1)==')'){
					if(name.charAt(name.length()-6)=='('){
						anio = name.substring(name.length()-5, name.length()-1);
					}
					else if(name.charAt(name.length()-6)=='-'){
						anio = name.substring(name.length()-10, name.length()-6);
					}
					else if(name.charAt(name.length()-2)=='-'){
						anio = name.substring(name.length()-6, name.length()-2);
					}
				}
				ILista<String> generos = new DoubleLinkedList<>(null);
				for(int i=0 ; i<generosArr.length ; i++){
					generos.agregarElementoFinal(generosArr[i]);
				}

				VOPelicula actual = new VOPelicula();
				actual.setTitulo(titulo);
				actual.setAgnoPublicacion(Integer.parseInt(anio));
				actual.setGenerosAsociados(generos);
				misPeliculas.agregarElementoFinal(actual);

				line = in.readLine();
			}
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		} 		
	}

	public Pelicula[] darMuestra(int n){
		Pelicula[] resp = new Pelicula[n];
		for(int i=0;i<n;i++){
			VOPelicula voActual = misPeliculas.darElemento((int)(Math.random()*(misPeliculas.darNumeroElementos()-1)));
			Pelicula actual = new Pelicula();
			actual.setMovie(voActual);
			resp[i] = actual;
		}
		return resp;
	} 

	public ILista<Pelicula> darMuestraEnLista(int num){
		ILista<Pelicula> sample = new DoubleLinkedList<>(null);
		for(int i=0; i < num; i++){
			VOPelicula voActual = misPeliculas.darElemento((int)(Math.random()*(misPeliculas.darNumeroElementos()-1)));
			Pelicula pActual = new Pelicula();
			pActual.setMovie(voActual);
			sample.agregarElementoFinal(pActual);
		}
		return sample;
	}
}
