package model.logic;

import java.util.ArrayList;

import model.data_structures.ILista;

public class Ordenador {
	public <T extends Comparable<T>> void qsort(ArrayList<T> arr, int start, int end) {
		
		if (start < end) {
			int i = start, j = end;
			T pivot = arr.get((i + j) / 2);
			while (i <= j){
				while (arr.get(i).compareTo(pivot) < 0){
					i++;
				}
				while (pivot.compareTo(arr.get(j)) < 0){ 
					j--;
				}
				if ( i <= j){
					T temp = arr.get(i);
					arr.set(i,arr.get(j));
					arr.set(j, temp);
					i++;
					j--;
				}
			} 
			qsort(arr, start, j);
			qsort(arr, i, end);
		}
	}
	
	public <T extends Comparable<T>> void qsort(T[] arr, int a, int b) {
		
        if (a < b) {
            int i = a, j = b;
            T x = arr[(i + j) / 2];

            do {
                while (arr[i].compareTo(x) < 0) i++;
                while (x.compareTo(arr[j]) < 0) j--;

                if ( i <= j) {
                    T tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
                    i++;
                    j--;
                }

            } while (i <= j);

            qsort(arr, a, j);
            qsort(arr, i, b);
        }

    }
	
public <T extends Comparable<T>> void qsort(ILista<T> lst, int start, int end) {
		
		if (start < end) {
			int i = start, j = end;
			T pivot = lst.darElemento((i + j) / 2);
			while (i <= j){
				while (lst.darElemento(i).compareTo(pivot) < 0){
					i++;
				}
				while (pivot.compareTo(lst.darElemento(j)) < 0){ 
					j--;
				}
				if ( i <= j){
					T temp = lst.darElemento(i);
					lst.set(i,lst.darElemento(j));
					lst.set(j, temp);
					i++;
					j--;
					
				}
			} 
			qsort(lst, start, j);
			qsort(lst, i, end);
		}
	}

	public ArrayList<Pelicula> ordenarPeliculas(ArrayList<Pelicula> pArreglo){
		ArrayList<Pelicula>resp = pArreglo;
		qsort(resp, 0, pArreglo.size()-1);
		return resp;
	}
	
	public ILista<Pelicula> ordenarPeliculas(ILista<Pelicula> pLista){
		ILista<Pelicula> resp = pLista;
		qsort(resp, 0, pLista.darNumeroElementos()-1);
		return resp;
	}
	
	public Pelicula[] ordenarPeliculas(Pelicula[] pArreglo){
		Pelicula[] resp = pArreglo;
		qsort(resp, 0, pArreglo.length-1);
		return resp;
	} 

}
